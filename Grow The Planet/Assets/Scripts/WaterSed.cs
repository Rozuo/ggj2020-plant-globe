﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSed : MonoBehaviour
{    
    private PlayerStates playerState;

    private float waitTime = 1f;
    private float currTime = 0f;
    public bool isWaterable = false;
    private GameObject[] seeds;

    public float maxWaterDistance = 10.0f;
    // Start is called before the first frame update
    void Start()
    {
        playerState = GetComponent<PlayerStates>();
    }

    // Update is called once per frame
    void Update()
    {
        currTime += Time.deltaTime;

        seeds = GameObject.FindGameObjectsWithTag("Plant");
        GameObject closestSeed = null;
        float minDistance = float.PositiveInfinity;
        foreach (GameObject seed in seeds)
        {
            float distance = Vector3.Distance(transform.position, seed.transform.position);
            if (distance < minDistance)
            {
                minDistance = distance;
                closestSeed = seed;
            }
        }
        if (minDistance <= maxWaterDistance)
        {
            isWaterable = true;
            
            float fire = Input.GetAxis("Fire3");
            if(fire > 0 && currTime >= waitTime && playerState.state != PlayerStates.playerState.Asleep)
            {
                closestSeed.GetComponent<SeedStates>().waterPlant();
                currTime = 0f;
            }
        }
        else
        {
            isWaterable = false;
        }
    }

}
