﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    private GameObject[] buildings;
    private GameObject[] plants;

    private float generationTime = 0;
    [SerializeField]
    private float totalO2 = 25f;
    [SerializeField]
    private float totalCO2 = 75f;
    [SerializeField]
    private float sum;
    private float maxO2 = 100f;
    private float maxCO2 = -100f;
    [SerializeField]
    private float constant = 6.8f;
    [SerializeField]
    private float rate;
    public float barFill;

    // Update is called once per frame
    void Update()
    {
        calculateCO2();
        calculateO2();
        generationTime += Time.deltaTime;
        if(generationTime > 1f)
        {
            sum = totalO2 - totalCO2;
            Debug.Log(sum);
            rate = totalO2 + (sum / constant) * Time.deltaTime;
            Debug.Log("Rate: " + rate);
            if (rate > maxO2)
            {
                Debug.Log("Win");
            }
            else if (rate < maxCO2)
            {
                Debug.Log("Lose");
            }
            generationTime = 0.0f;
            barFill = barFillVal(rate);
        }
    }

    void calculateCO2()
    {
        buildings = GameObject.FindGameObjectsWithTag("Building");
        if(buildings.Length <= 0)
        {
            Debug.Log("LOSE by lack of buildings");
        }
        totalCO2 += buildings.Length*4;
    }

    void calculateO2()
    {
        plants = GameObject.FindGameObjectsWithTag("Plant");
        SeedStates currentSeed;
        for (int i = 0; i < plants.Length; i++)
        {
            currentSeed = plants[i].GetComponent<SeedStates>();
            switch (currentSeed.state)
            {
                case (SeedStates.PlantStates.Stage1):
                    totalO2 += 1;
                    break;
                case (SeedStates.PlantStates.Stage2):
                    totalO2 += 2;
                    break;
                case (SeedStates.PlantStates.Stage3):
                    totalO2 += 5;
                    break;
                default:
                    break;
            }
        }
    }

    public float barFillVal(float val)
    {
        return (val + maxO2) / (maxO2 + -(maxCO2));
    }
}
