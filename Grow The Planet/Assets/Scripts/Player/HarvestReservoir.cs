﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HarvestReservoir : MonoBehaviour
{
    private PlayerStates playerState;

    private PlayerWater playerWater;
    private PlayerPower playerPower;
    public float energyUsage = 20.0f;

    public float lightBrightness;
    private float maxBrightness = 20f;
    public float maxLightDistance;

    public float reservoirCapacity = 50.0f;
    private float waitTime = 1f;
    private float currTime = 0f;
    private GameObject[] reservoirs;
    private Light theLight;

    // Start is called before the first frame update
    void Start()
    {
        reservoirs = GameObject.FindGameObjectsWithTag("Reservoir");
        theLight = GetComponentInChildren<Light>();
        playerState = GetComponent<PlayerStates>();
        playerPower = GetComponent<PlayerPower>();
        playerWater = GetComponent<PlayerWater>();
    }

    // Update is called once per frame
    void Update()
    {
        GameObject closestReservoir = findClosestReservoir();
        float reservoirDistance = Vector3.Distance(transform.position, closestReservoir.transform.position);
        lightBrightness = Mathf.Min(0.0f, (1.0f - ((maxLightDistance - reservoirDistance) / maxLightDistance)));
        theLight.intensity = lightBrightness*maxBrightness;
        currTime += Time.deltaTime;
        float fire = Input.GetAxis("Fire2");
        if(fire > 0 && currTime >= waitTime && playerState.state != PlayerStates.playerState.Asleep)
        {
            if (lightBrightness >= 0.1)
            {
                playerWater.AddWater(reservoirCapacity * lightBrightness);
                Destroy(closestReservoir);
            }
            playerPower.usePower(energyUsage);
            currTime = 0f;
        }
    }

    GameObject findClosestReservoir()
    {
        GameObject closestReservoir = reservoirs[0];
        float minDistance = float.PositiveInfinity;
        foreach (GameObject reservoir in reservoirs)
        {
            float distance = Vector3.Distance(transform.position, reservoir.transform.position);
            if (distance < minDistance)
            {
                minDistance = distance;
                closestReservoir = reservoir;
            }
        }
        return closestReservoir;
    }
    
}
