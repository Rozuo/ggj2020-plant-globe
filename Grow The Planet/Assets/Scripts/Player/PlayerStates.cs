﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStates : MonoBehaviour
{
    public enum playerState
    {
        Idle, Moving, Asleep
    }

    public playerState state;

    private void Awake()
    {
        state = playerState.Idle;
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        if (state == playerState.Asleep)
        {
            return;
        }
        else if(horizontal > 0 || horizontal < 0)
        {
            state = playerState.Moving;
        }
        else
        {
            state = playerState.Idle;
        }
    }
}
