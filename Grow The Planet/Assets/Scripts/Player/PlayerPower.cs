﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPower : MonoBehaviour
{
    public float maxPower = 100;
    public float powerRegenRate = 10;
    public float sleepTime = 4;
    private float power;
    private float powerRegenPerStep;
    private int sleepTimeSteps;
    // Start is called before the first frame update
    private int fixedStep = 0;
    private int sleepStepsRemaining;
    void Start()
    {
        powerRegenPerStep = powerRegenRate * (1 / Time.fixedDeltaTime);
        sleepTimeSteps = Mathf.RoundToInt(sleepTime * (1 / Time.fixedDeltaTime));
    }

    void FixedUpdate()
    {
        fixedStep += 1;
        power = Mathf.Min(maxPower, power + powerRegenPerStep);

        if (power <= 0){
            sleepStepsRemaining = sleepTimeSteps;
        }
        sleepStepsRemaining -= 1;
        sleepStepsRemaining = sleepStepsRemaining > 0 ? sleepStepsRemaining : 0; // int max
        if (sleepStepsRemaining > 0)
        {
            this.GetComponent<PlayerStates>().state = PlayerStates.playerState.Asleep;
        }
        else if (this.GetComponent<PlayerStates>().state == PlayerStates.playerState.Asleep){
            this.GetComponent<PlayerStates>().state = PlayerStates.playerState.Idle;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void usePower(float Amount)
    {
        power -= Amount;
    }
}
