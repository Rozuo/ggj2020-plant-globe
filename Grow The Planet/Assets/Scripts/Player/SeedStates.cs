﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedStates : MonoBehaviour
{

    Transform planet;
    Vector3 gravity;
    public float buildingDamageDistance;
    public float buildingDamagePerSecond;
    public float massModifier;
    public float throwForce;
    public GameObject bigVine;
    public GameObject vine; 

    public bool isWatered;
    private Rigidbody rb;
    
    public enum PlantStates
    {
        Stage0, Stage1, Stage2, Stage3
    }

    public PlantStates state;


    private float finalStageTime;
    private float currTime = 0f;

    // Start is called before the first frame update
    void Start()
    {
        planet = GetComponent<Transform>().parent;
        rb = GetComponent<Rigidbody>();
        gravity = Vector3.zero;


        state = PlantStates.Stage0;
        finalStageTime = 30f;
    }

    // Update is called once per frame
    void Update()
    {

        if (isWatered)
        {
            currTime += Time.deltaTime;
        }
        if (currTime >= 20f && state == PlantStates.Stage1)
        {
            Instantiate(vine, this.gameObject.transform);
            GetComponent<Renderer>().enabled = false; 
            state = PlantStates.Stage2;
            isWatered = false;
        }
        else if (currTime >= finalStageTime && state == PlantStates.Stage2)
        {   
            foreach (Transform child in transform)
            {
                
                Destroy(child.gameObject);

            }

            Instantiate(bigVine, this.gameObject.transform);

            state = PlantStates.Stage3;
        }
    }


    private void FixedUpdate()
    {
        //Physics to move the seed towards the planet, only when it is in stage 0 (still a seed)
        if (state == PlantStates.Stage0)
        {
            gravity = planet.position - transform.position;

            rb.AddForce(gravity * massModifier);// + transform.right * throwForce);
        }
        else if (state == PlantStates.Stage3)
        {
            GameObject[] buildings = GameObject.FindGameObjectsWithTag("Building");
            foreach (GameObject building in buildings)
            {
                if (building != null)
                {
                    if (Vector3.Distance(building.transform.position, this.transform.position) <= buildingDamageDistance)
                    {
                        building.GetComponent<BuildingHealth>().takeDamage(buildingDamagePerSecond * Time.deltaTime);
                    }
                }
            }

        }

    }

    // When the seed collides with the world, change its state to 1 (smallest plant) and freeze it's position
    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.name == "World")
        {
            state = PlantStates.Stage1;
            rb.constraints = RigidbodyConstraints.FreezeAll;
            transform.rotation = Quaternion.FromToRotation(Vector3.up, transform.position);

        }
    }

    public void waterPlant()
    {
        isWatered = true;
    }
}
