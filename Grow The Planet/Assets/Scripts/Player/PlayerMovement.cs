﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private float moveSpeed = 0.2f;

    private Transform world;

    public PlayerStates playerState;
    // Start is called before the first frame update
    void Awake()
    {
        world = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {  
        if (playerState.state != PlayerStates.playerState.Asleep)
        {

            float movement = Input.GetAxis("Horizontal");

            movement *= -moveSpeed;

            Vector3 rotation = new Vector3(movement, 0, 0);

            world.Rotate(rotation); //Quaternion.Slerp(world.rotation, rotation, Time.deltaTime);
        }
    }
}
