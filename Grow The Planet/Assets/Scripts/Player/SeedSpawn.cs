﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedSpawn : MonoBehaviour
{
    public GameObject prefabSeed;
    public GameObject world;

    public GameObject player;
    private PlayerStates playerState;
    private PlayerPower playerPower;
    public float energyUsage = 20.0f;
    private Transform spawnPoint;
    private float waitTime = 1f;
    private float currTime = 0f;

    // Start is called before the first frame update
    void Start()
    {
        spawnPoint = GetComponent<Transform>();
        playerState = player.GetComponent<PlayerStates>();
        playerPower = player.GetComponent<PlayerPower>();
    }

    // Update is called once per frame
    void Update()
    {
        currTime += Time.deltaTime;
        float fire = Input.GetAxis("Fire1");
        if(fire > 0 && currTime >= waitTime && playerState.state != PlayerStates.playerState.Asleep)
        {
            GameObject gO = Instantiate(prefabSeed);
            Transform gOPostion = gO.GetComponent<Transform>();
            gOPostion.position = new Vector3(spawnPoint.position.x, spawnPoint.position.y, 0);
            gO.transform.parent = world.transform;
            playerPower.usePower(energyUsage);
            currTime = 0f;
        }
        
    }
}
