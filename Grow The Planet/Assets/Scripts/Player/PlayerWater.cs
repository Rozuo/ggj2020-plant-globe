﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWater : MonoBehaviour
{

    public float maxWater;
    public float startWater;
    private float water;
    // Start is called before the first frame update
    void Start()
    {
        water = startWater;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddWater(float amount)
    {
        water = Mathf.Min(water + amount, maxWater);
    }
    public void UseWater(float amount)
    {
        water = Mathf.Max(water - amount, 0.0f);
    }
}
