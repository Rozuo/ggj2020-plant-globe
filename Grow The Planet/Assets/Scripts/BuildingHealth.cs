﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingHealth : MonoBehaviour
{
    public float maxHealth;
    private float health;


    public GameObject[] buildings;  // must be of size two, and where 0 is the undamaged building and 1 is the damaged building
    public GameObject vineChild;
    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(health < maxHealth)
        {
            Debug.Log("Hello there");
            vineChild.SetActive(true);
        }
        else if(health < maxHealth / 2)
        {
            buildings[1].SetActive(true);
            buildings[0].SetActive(false);
        }
    }

    public void takeDamage(float damage)
    {
        health = Mathf.Max(health - damage, 0);
        
        if (health == 0)
        {
            Destroy(this.gameObject);
        }
    }

}
