﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    public GameObject[] buildings;
    public GameObject reservoir;
    public Transform worldSphere;

    public int wave = 0;
    public int waveLengthSeconds = 60;

    public int buildingsPerWaveBase = 3;
    public float buildingsPerWaveMultiplier = 1.5f;

    public int reservoirsPerWaveBase = 3;

    public float reservoirsPerWaveMultiplier = 1.5f;

    private int waveLengthSteps;
    private int fixedStep = -1;
    // Start is called before the first frame update
    void Start()
    {
        waveLengthSteps = waveLengthSeconds * Mathf.RoundToInt(1.0f / Time.fixedDeltaTime);
    }

    void FixedUpdate()
    {
        fixedStep += 1;
        if ((fixedStep % waveLengthSteps) == 0)
        {
            wave += 1;
            SpawnBuildings();    
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnBuildings()
    {
        int numBuildings = buildingsPerWaveBase * Mathf.RoundToInt(wave * buildingsPerWaveMultiplier); // TODO: Maybe floor this instead?
        for (int i = 0; i < numBuildings; ++i)
        {
            int buildingIndex = Random.Range(0, buildings.Length);
            Vector3 location =  Random.onUnitSphere *  100.0f; //worldSphere.GetComponent<Renderer>().bounds.extents.magnitude;
            Instantiate(buildings[buildingIndex], location, Quaternion.FromToRotation(Vector3.up, location), worldSphere);
        }
    }

    void SpawnReservoirs()
    {
        int numReservoirs = reservoirsPerWaveBase * Mathf.RoundToInt(wave * reservoirsPerWaveMultiplier); // TODO: Maybe floor this instead?
        for (int i = 0; i < numReservoirs; ++i)
        {
            Vector3 location =  Random.onUnitSphere *  100.0f; //worldSphere.GetComponent<Renderer>().bounds.extents.magnitude;
            Instantiate(reservoir, location, Quaternion.FromToRotation(Vector3.up, location), worldSphere);
        }

    }
}


