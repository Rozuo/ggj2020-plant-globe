﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OxygenBarBehavior : MonoBehaviour
{

    private Image bar;
    
    private GameObject theManager;
    private GameManager managerBehavior;

    private float changeRate = 10f;
    // Start is called before the first frame update
    void Start()
    {
        bar = GetComponent<Image>();
        theManager = GameObject.FindGameObjectWithTag("GameManager");
        managerBehavior = theManager.GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        bar.fillAmount = managerBehavior.barFill;
    }
}
